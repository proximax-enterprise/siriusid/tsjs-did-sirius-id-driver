FROM node:10.5

# Create Directory for the Container
WORKDIR /app

# Only src
COPY . /app

# Install all Packages
RUN yarn install

# Execute
CMD [ "yarn", "server" ]
EXPOSE 8080