# tsjs-did-siriusid-driver

Universal Resolver DID Driver for the `did:sirius` identity

## Example DID

```ts
did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK
```

## Example request

```curl
curl -X GET http://localhost:8080/1.0/identifiers/did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK
```

## Get Started

Clone this repository

```git
git clone https://gitlab.com/proximax-enterprise/siriusid/tsjs-did-sirius-id-driver
```

Install all packages

```yarn
yarn install
```

Run server locally

```yarn
yarn server
```

Test

```curl
curl -X GET http://localhost:8080/1.0/identifiers/did:sirius:2VhYrbauc2cCx9ZpCp5wrDtK7HKf7jrsvgoKBD4KgK
```

## Docker

Build docker

```docker
docker build -f ./docker/Dockerfile . -t proximax/tsjs-did-sirius-id-driver
```

Run the docker container

```docker
docker run -it --rm --name tsjs-did-sirius-id-driver -p 8080:8080 proximax/tsjs-did-sirius-id-driver
```

Get the image

```docker
docker pull proximax/tsjs-did-sirius-id-driver
```
