import express from 'express'
import { Resolver } from 'did-resolver';
import { getResolver } from 'tsjs-did-siriusid-resolver';

const PORT = 8080;

const app = express();
app.get('/1.0/identifiers/:did', async (req, res) => {
  const did = req.params.did;
  console.log(did);

  const resolver = new Resolver(getResolver());
  try {
    const didDocument = await resolver.resolve(did);
    if (didDocument)
      res.send(didDocument);
    else
      res.sendStatus(404)

  } catch (error) {
    res.sendStatus(404)
  }

});

app.listen(PORT, function () {
  console.log(`SiriusId Resolver driver is running on port ${PORT}...`)
});