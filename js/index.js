"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const did_resolver_1 = require("did-resolver");
const tsjs_did_siriusid_resolver_1 = require("tsjs-did-siriusid-resolver");
const PORT = 8080;
const app = express_1.default();
app.get('/1.0/identifiers/:did', async (req, res) => {
    const did = req.params.did;
    console.log(did);
    const resolver = new did_resolver_1.Resolver(tsjs_did_siriusid_resolver_1.getResolver());
    try {
        const didDocument = await resolver.resolve(did);
        if (didDocument)
            res.send(didDocument);
        else
            res.sendStatus(404);
    }
    catch (error) {
        res.sendStatus(404);
    }
});
app.listen(PORT, function () {
    console.log(`SiriusId Resolver driver is running on port ${PORT}...`);
});
